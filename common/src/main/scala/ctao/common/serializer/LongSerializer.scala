package ctao.common.serializer

import java.util

import org.apache.kafka.common.serialization.Serializer

/**
  * Created by ctao on 16-1-26.
  */
class LongSerializer extends Serializer[Long] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()

  override def serialize(topic: String, data: Long): Array[Byte] = BigInt(data).toByteArray

  override def close(): Unit = ()
}
