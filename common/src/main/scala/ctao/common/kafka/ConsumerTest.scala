package ctao.common.kafka

import ctao.common.util.{KafkaConsumerConfig, KafkaProducerConfig}
import kafka.utils.ShutdownableThread
import org.apache.kafka.clients.consumer.KafkaConsumer
import scala.collection.JavaConversions._
import scala.util.{Success, Try}

/**
  * Created by ctao on 16-1-26.
  */
object ConsumerTest extends App {


  val a = new Consumer
  a.doWork()
}


class Consumer extends ShutdownableThread("a") {
  val consumer: KafkaConsumer[String, Long] = new KafkaConsumer[String, Long](KafkaConsumerConfig())

  override def doWork(): Unit = {
    consumer.subscribe(Seq("mytest"))
    val reacords = consumer.poll(1000)
    reacords.foreach(println)
  }
}
