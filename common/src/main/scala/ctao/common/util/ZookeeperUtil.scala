package ctao.common.util

import java.util.Properties

import org.I0Itec.zkclient.ZkClient
import org.I0Itec.zkclient.serialize.ZkSerializer

/**
  * Created by ctao on 16-1-25.
  */
object ZookeeperUtil {

  private val defaultSessionTimeOut = 10000
  private val defaultConnectionTimeOut = 10000
  def createClient(config: Properties = KafkaProducerConfig(),
                   sessionTimeOut: Int = defaultSessionTimeOut,
                   connectionTimeOut: Int = defaultConnectionTimeOut,
                   serializer: ZkSerializer): ZkClient = {
    val host = config.getProperty("zookeeper.connect")
    new ZkClient(host, sessionTimeOut, connectionTimeOut, serializer)
  }
}
