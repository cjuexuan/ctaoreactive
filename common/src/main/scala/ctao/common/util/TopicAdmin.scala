package ctao.common.util

import java.util.Properties

import kafka.admin.AdminUtils
import kafka.utils.ZkUtils

/**
  * Created by ctao on 16-1-25.
  */
case class TopicAdmin(zkUtils: ZkUtils) {

  private val defaultPartitionNum = 1
  private val defaultReplicationFactor = 1

  def createTopic(name: String,
                  partitionNum: Int = defaultPartitionNum,
                  replicationFactor: Int = defaultReplicationFactor,
                  config: Properties = new Properties()): Unit = {
    AdminUtils.createTopic(zkUtils, name, partitionNum, replicationFactor, config)
  }

  def deleteTopic(name: String): Unit = {
    AdminUtils.deleteTopic(zkUtils, name)
  }
}
