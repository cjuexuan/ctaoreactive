package ctao.common.deserializer

import java.util

import org.apache.kafka.common.serialization.Deserializer

/**
  * Created by ctao on 16-1-26.
  */
class LongDeserializer extends Deserializer[Long]{
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()

  override def close(): Unit = ()

  override def deserialize(topic: String, data: Array[Byte]): Long = BigInt(data).toLong
}