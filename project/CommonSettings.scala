import sbt.Keys._
import sbt._
import sbt.plugins.JvmPlugin

object CommonSettings extends AutoPlugin {

  override def requires: JvmPlugin.type = plugins.JvmPlugin


  override def trigger: PluginTrigger = allRequirements


  override def projectSettings: Seq[Def.Setting[Boolean]] = Seq(
    parallelExecution in Test := false,
    //    resolvers ++= Dependencies.resolvers,
    fork in Test := true,
    fork in run := true
  )


}
