import sbt._
import sbt.Keys._


object Dependencies {

  val core = Seq(
    Library.jline excludeAll ExclusionRule(name = "slf4j-log4j12"),
    Library.`scala-async`,
    Library.logback,
    Library.slf4j excludeAll ExclusionRule(name = "slf4j-log4j12"),
    Library.`play-json` excludeAll ExclusionRule(name = "slf4j-log4j12"),
    Library.scalatest excludeAll ExclusionRule(name = "slf4j-log4j12") ,
    Library.config excludeAll ExclusionRule(name = "slf4j-log4j12")
  )

  val project = Seq(
    Library.jline,
    Library.slf4j,
    Library.logback,
    Library.`nop-slf4j`
  )


  val resolvers = DefaultOptions.resolvers(snapshot = true) ++ Seq(
    "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
  )

  //  val resolvers = DefaultOptions.resolvers(snapshot = true) ++ Seq(
  //    "scalaz-releases" at "http://dl.bintray.com/scalaz/releases" // play-test -> specs2 -> scalaz-stream
  //  )
}


object Version {

  val akkaStream = "2.0.1"

  val reactiveKafka = "0.9.0"

  val scala = "2.11.7"

  val `play-json` = "2.4.6"

  val jline = "2.12.1"

  val logback = "1.1.3"

  val `akka-slf4j` = "2.3.12"

  val slf4j = "1.7.14"

  val `scala-async` = "0.9.5"

  val scalatest = "3.0.0-M14"

  val log4j = "1.2.17"

  val config = "1.3.0"

  val kafka = "0.9.0.0"
}


object Library {
  val akkaStream = "com.typesafe.akka" % "akka-stream-experimental_2.11" % Version.akkaStream

  val reactiveKafka = "com.softwaremill.reactivekafka" %% "reactive-kafka-core" % Version.reactiveKafka

  val `play-json` = "com.typesafe.play" % "play-json_2.11" % Version.`play-json`

  val jline = "jline" % "jline" % Version.jline

  val logback = "ch.qos.logback" % "logback-classic" % Version.logback

  val `akka-slf4j` = "com.typesafe.akka" %% "akka-slf4j" % Version.`akka-slf4j`

  val kafka = "org.apache.kafka" % "kafka_2.11" % Version.kafka exclude("javax.jms","jms") exclude("com.sun.jdmk","jmxtools") exclude("com.sun.jmx","jmxri")

//  val slf4j = "org.slf4j" % "log4j-over-slf4j" % Version.slf4j

  val slf4j = "org.slf4j" % "slf4j-api" % Version.slf4j

  val `nop-slf4j` = "org.slf4j" % "slf4j-nop" % Version.slf4j

  val `simple-slf4j` = "org.slf4j" % "slf4j-simple" % Version.slf4j

  val `scala-async` = "org.scala-lang.modules" % "scala-async_2.11" % Version.`scala-async`

  val scalatest = "org.scalatest" % "scalatest_2.11" % Version.scalatest

  val log4j = "log4j" % "log4j" % Version.log4j

  val config =  "com.typesafe" % "config" % Version.config

}