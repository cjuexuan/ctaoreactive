name := "CtaoReactive"

scalaVersion := Version.scala

libraryDependencies ++= Dependencies.project

val gitHeadCommitSha = taskKey[String]("determine the current git commit SHA")

val makeVersionProperties = taskKey[Seq[File]]("Makes a version.properties file.")


assemblyJarName in assembly := "ctao.reative.kafka"

mainClass in assembly := Some("ctao.reactive.kafka.Application")

packageOptions in assembly ~= { pos ⇒
  pos.filterNot { po ⇒
    po.isInstanceOf[Package.MainClass]
  }
}

inThisBuild(gitHeadCommitSha := Process("git rev-parse HEAD").lines.head)
lazy val common = preownedKittenProject("common").
  settings(
    makeVersionProperties := {
      val propFile = (resourceManaged in Compile).value / "version.properties"
      val content = s"version=${gitHeadCommitSha.value}"
      IO.write(propFile, content)
      Seq(propFile)
    },
    libraryDependencies ++= Seq(Library.kafka excludeAll ExclusionRule(name = "slf4j-log4j12")
      ,Library.`play-json` excludeAll ExclusionRule(name = "slf4j-log4j12"))
  ).settings(CommonSettings.projectSettings)

lazy val ctaokafka = preownedKittenProject("ctaokafka").
  dependsOn(common).
  settings(libraryDependencies ++= Seq(Library.reactiveKafka,Library.akkaStream,
    Library.`akka-slf4j`
  )).settings(CommonSettings.projectSettings)

/**
  * 创建通用模板
  *
  * @param name 工程名
  * @return
  */
def preownedKittenProject(name: String): Project = {
  Project(name, file(name)).
    settings(
      version := "0.1-SNAPSHOT",
      organization := "ctao",
      libraryDependencies ++= Dependencies.core,
      scalaVersion := Version.scala
    )
}


