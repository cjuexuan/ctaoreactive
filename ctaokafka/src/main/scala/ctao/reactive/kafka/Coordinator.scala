package ctao.reactive.kafka

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import scala.collection.mutable.ArrayBuffer
import Command._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Created by ctao on 16-1-23.
  */
class Coordinator extends Actor with ActorLogging {

  import Coordinator._

  val conf = ConfigFactory.load()


  val schedulerTime =conf.getInt("common.scheduler.time")

  val writerBuffer: ArrayBuffer[Option[ActorRef]] = new ArrayBuffer[Option[ActorRef]]()

  val readerBuffer: ArrayBuffer[Option[ActorRef]] = new ArrayBuffer[Option[ActorRef]]()

  //  var writer: Option[ActorRef] = None
  //  var reader: Option[ActorRef] = None
  lazy val mat = ActorMaterializer()(context)

  override def receive: Receive = {
    case msg@InitialMessage(StartNumber(num), mode) ⇒
      log.debug(s"Starting the numbers coordinator with $msg")
      buildWriteOrBuildRead(mode, num)

    case msg: InitialMessage ⇒
      log.error(s"Did not understand $msg")
      log.error("shutdown")
      context.system.shutdown()

    case ReadInitialized(actorRef) ⇒
      log.debug(s"Reader initialized :${actorRef.path.toString}")
      context.system.scheduler.scheduleOnce(schedulerTime.seconds, self, Stop(actorRef))
      log.debug(s"end scheduler stop ${actorRef.path.toString}")
    case WriteInitialized(actorRef) ⇒
      log.debug(s"Writer initialized:${actorRef.path.toString}")
      context.system.scheduler.scheduleOnce(schedulerTime.seconds, self, Stop(actorRef))
      log.debug(s"end scheduler stop ${actorRef.path.toString}")
    case Stop(actorRef) ⇒
      log.debug("Stopping the coordinator")

      writerBuffer -= Some(actorRef)
      readerBuffer -= Some(actorRef)

      log.debug(s"writeBuffer.length ${writerBuffer.length} and readerBuffer.length ${readerBuffer.length}")

      if (writerBuffer.isEmpty && readerBuffer.isEmpty) {
        context.system.scheduler.scheduleOnce(1.seconds, self, Shutdown)
      }
    case Shutdown ⇒
      log.debug("Shutting down the app")
      context.system.shutdown()
      log.info("shutdown the app")
  }


  def buildWriteOrBuildRead(mode: Mode, numActor: Int): Unit = mode match {

    case Mode.Write ⇒
      log.debug("write mode")
      (1 to numActor).foreach { x ⇒
        val writer = Some(context.actorOf(NumberProducer.props, name = s"writerActor-$x"))
        writerBuffer += writer
      }
    case Mode.Read ⇒
      log.debug("read mode")
      (1 to numActor).foreach { x ⇒
        val reader = Some(context.actorOf(NumberConsumer.props, name = s"readerActor-$x"))
        readerBuffer += reader
      }
    case Mode.Readwrite ⇒
      log.debug("readwrite mode")
      (1 to numActor).foreach { x ⇒
        val writer = Some(context.actorOf(NumberProducer.props, name = s"writerActor-$x"))
        val reader = Some(context.actorOf(NumberConsumer.props, name = s"readerActor-$x"))
        writerBuffer += writer
        readerBuffer += reader
      }


  }
}


case object Coordinator {

  case class InitialMessage(name: Command, mode: Mode)

}
