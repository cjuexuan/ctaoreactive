package ctao.reactive.kafka

/**
  * Created by ctao on 16-1-23.
  */
sealed trait Mode

object Mode {
  case object Read extends Mode
  case object Write extends Mode
  case object Readwrite extends Mode
}
