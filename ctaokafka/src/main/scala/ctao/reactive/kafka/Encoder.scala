package ctao.reactive.kafka


import org.apache.kafka.common.serialization.Deserializer
import play.api.libs.json.{Reads, Json, Writes}

/**
  * Created by ctao on 16-1-23.
  */

trait EncoderInstance {

  import kafka.serializer.Encoder

  implicit def encoder[A](implicit W: Writes[A]): Encoder[A] = new Encoder[A] {
    override def toBytes(a: A): Array[Byte] = Json.toJson(a).toString.getBytes("UTF-8")
  }
}


trait DecoderInstance {

  import kafka.serializer.Decoder

  implicit def decoder[A](implicit R: Reads[A]): Decoder[A] = new Decoder[A] {
    override def fromBytes(bytes: Array[Byte]): A = Json.parse(bytes).as[A]
  }
}

object Encoder extends EncoderInstance {

  import kafka.serializer.Encoder

  def apply[A](implicit E: Encoder[A]): Encoder[A] = E
}

object Decoder extends DecoderInstance {

  import kafka.serializer.Decoder

  def apply[A](implicit D: Decoder[A]): Decoder[A] = D

  def apply[A](topic: String, deserializer: Deserializer[A]): Decoder[A] = new Decoder[A] {
    override def fromBytes(bytes: Array[Byte]): A = deserializer.deserialize(topic, bytes)
  }
}
