package ctao.reactive.kafka

import akka.actor.ActorRef

/**
  * Created by ctao on 16-1-23.
  */
sealed trait Command

object Command {

  case class ReadInitialized(actorRef: ActorRef) extends Command

  case class WriteInitialized(actorRef: ActorRef) extends Command

  case class Stop(actorRef: ActorRef) extends Command

  case object Shutdown extends Command

  case class StartNumber(num: Int) extends Command

  case object StartConsume extends Command

  case object StartProduce extends Command

}

