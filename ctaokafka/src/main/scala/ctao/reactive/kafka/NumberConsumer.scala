package ctao.reactive.kafka

import akka.actor.SupervisorStrategy.Escalate
import akka.actor._
import com.typesafe.config.ConfigFactory
import ctao.common.deserializer.LongDeserializer
import ctao.common.util.KafkaConsumerConfig
import ctao.reactive.kafka.Command._
import ctao.reactive.kafka.NumberConsumer.Consume
import kafka.consumer.{Consumer, ConsumerConfig, ConsumerConnector, KafkaStream}
import kafka.message.MessageAndMetadata
import org.apache.kafka.common.serialization.StringDeserializer

import scala.async.Async._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
import scala.util.{Failure, Success, Try}

/**
  * Created by ctao on 16-1-23.
  */
class NumberConsumer extends Actor with ActorLogging {

  val conf = ConfigFactory.load()
  private var consumer: Try[ConsumerConnector] = _
  val threadNum = conf.getInt("common.threadNum")
  private val execution = java.util.concurrent.Executors.newFixedThreadPool(threadNum)
  val topic = conf.getString("consumer.topic")

  private var streams: Option[List[KafkaStream[String, Long]]] = None

  override def receive: Receive = {
    case StartConsume ⇒ consumer.foreach { (consumer: ConsumerConnector) ⇒
      val consumerMap = consumer.createMessageStreams(Map(topic → 1)
        , Decoder(topic, new StringDeserializer),
        Decoder(topic, new LongDeserializer))

      streams = Option(consumerMap(topic))
      if (streams.isDefined) {
        log.info(s"Got streams ${streams.get.length} $streams")
        streams.get.foreach { kafkaStream ⇒
          self ! Consume(kafkaStream)

        }
      }
    }

    case Consume(kafkaStream) ⇒
      log.info(s"Handling KafkaStream ${kafkaStream.clientId}")
      implicit val executionContextExecutor: ExecutionContextExecutor = ExecutionContext.fromExecutor(execution)
      async {
        kafkaStream.iterator().foreach {
          case msg: MessageAndMetadata[String, Long] ⇒
            log.info(s"${self.path.toString} : kafkaStream ${kafkaStream.clientId} " +
              s" received offset ${msg.offset}  partition ${msg.partition} value ${msg.message}")
        }
      }


  }


  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case e: Exception ⇒
      //handle failing kafka
      log.error(s"Read failed $e")
      Escalate
  }


  override def preStart(): Unit = {
    super.preStart()
    consumer = Try(Consumer.create(consumerConfig))
    consumer match {
      case Success(c) ⇒ context.parent ! ReadInitialized(self)
        self ! StartConsume
      case Failure(e) ⇒
        log.error(e, "Could not create kafkaConsumer")
        context.parent ! Shutdown
    }


  }


  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    if (streams.isDefined) {
      log.debug("cleaning streams")
      streams.get.foreach(_.clear())
      log.debug("cleaned streams")
    }
    log.debug("stopping all consumer")
    consumer.foreach(_.shutdown())
    log.debug("stop all consumer")
    log.debug("shutting down execution")
    execution.shutdown()
    log.debug("shutdown execution")
  }

  private val consumerConfig: ConsumerConfig = new ConsumerConfig(KafkaConsumerConfig())

}


object NumberConsumer {

  def props: Props = Props(new NumberConsumer())

  private case class Consume(kafkaStream: KafkaStream[String, Long])

}