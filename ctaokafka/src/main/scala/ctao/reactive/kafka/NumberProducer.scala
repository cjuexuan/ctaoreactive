package ctao.reactive.kafka

import akka.actor.SupervisorStrategy.Resume
import akka.actor._
import akka.event.LoggingReceive
import com.typesafe.config.ConfigFactory
import ctao.common.util.KafkaProducerConfig
import ctao.reactive.kafka.Command._
import org.apache.kafka.clients.producer._


/**
  * Created by ctao on 16-1-23.
  */
class NumberProducer extends Actor with ActorLogging {
  private val conf = ConfigFactory.load()
  val numMessage = conf.getInt("common.numMessage")

  val topic = conf.getString("consumer.topic")


  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case e: Exception ⇒
      //can handle failing here
      log.error(s"Write failed $e")
      Resume
  }

  private var producer: KafkaProducer[String, Long] = _


  override def preStart(): Unit = {
    producer = initProducer()
    context.parent ! WriteInitialized(self)
    self ! StartProduce
  }

  override def receive: Receive = LoggingReceive {
    case StartProduce ⇒ produce(producer, numMessage)
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    log.debug("closing producer")
    producer.close()
    log.debug("closed producer")
  }


  private def produce(producer: KafkaProducer[String, Long], numMessage: Int): Unit = {
    (1 to numMessage).foreach { messageNum ⇒
      val message = new ProducerRecord[String, Long](topic, (messageNum + 1).toString, messageNum)
      producer.send(message, new Callback {
        override def onCompletion(metadata: RecordMetadata, exception: Exception): Unit = {
          val maybeMetadata = Option(metadata)
          val maybeException = Option(exception)
          if (maybeMetadata.isDefined) {
            log.info(s"actor ${self.path.toString}: $messageNum onCompletion offset ${metadata.offset},partition ${metadata.partition}")
          }
          if (maybeException.isDefined) {
            log.error(exception, s"$messageNum onCompletion received error")
          }
        }
      })


    }
  }


  private def initProducer(): KafkaProducer[String, Long] = {
    log.debug(s"Config ${KafkaProducerConfig()}")
    new KafkaProducer[String, Long](KafkaProducerConfig())

  }
}


object NumberProducer {
  def props: Props = Props(new NumberProducer)
}