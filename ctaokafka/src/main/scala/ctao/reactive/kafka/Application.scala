package ctao.reactive.kafka

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import ctao.reactive.kafka.Coordinator.InitialMessage
import ctao.reactive.kafka.Mode.{Read, Readwrite, Write}
import org.slf4j.LoggerFactory

/**
  * Created by ctao on 16-1-23.
  */
object Application extends App {

  val log = LoggerFactory.getLogger(this.getClass)

  val system = ActorSystem("Kafka")

  val coordinator = system.actorOf(Props(new Coordinator), name = "coordinator")


  import Command._

  val conf = ConfigFactory.load()
  private val numActor = conf.getInt("common.actor")
  log.info(s"start app")


  val appMode = conf.getString("common.mode") match {
    case s:String  ⇒ s.toUpperCase match {
      case "READ" ⇒ Read
      case "WRITE" ⇒ Write
      case "READWRITE" ⇒ Readwrite
    }
    case _ ⇒ throw  new IllegalArgumentException("can't load mode")
  }


  coordinator ! InitialMessage(StartNumber(numActor),appMode)

}
