#ctaokafka
ctaokafka是[reactive-kafka-scala](https://github.com/inoio/reactive-kafka-scala)中的demo
我做了如下下修改：
* 第一，修改成了多actor的方式
* 使用了typesafe.config的配置方式
* 里面包含一些sbt的实践
* 修复了由于kafka consumer造成的死锁问题
* 在编码规范上有了进一步的要求，改掉了一些接口，并封装了kafkaConfig在common中